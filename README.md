# ASR-PRO-U8G2
1修改 asr_pro_sdk\projects\cwsl_sample\project_file\source_file.prj 添加 U8G2的源文件 头文件

2在D:\天问Block\asrpro\asr_pro_sdk\ 下添加U8G2文件夹及内容
   D:\天问Block\asrpro下添加U8G2文件夹及内容
（直接下载u8g2202405.rar)

3引脚定义 VCC->VCC GND->GND 
PA
#define MOSI 5
#define SCK 6
#define DC 3
#define CS 2

使用 串口测试程序.hd

在终端打开 ./rebuild 

make生成的文件地址为D:\天问Block\asrpro\asr_pro_sdk\projects\cwsl_sample\project_file\build

------------------------------------------------------------------------------------------------
移植步骤： 参考网上u8g2的移植代码 （使用ssd1306 soft spi 模式，软件模拟）
修改核心代码以适应asrpro 

uint8_t u8x8_avr_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
  {
	// Re-use library for delays

	switch(msg)
	{
	  //ms
	  	case U8X8_MSG_DELAY_MILLI:
        delay(1);
      break;
      
      //10 us
      case U8X8_MSG_DELAY_10MICRO:
        delay10us();
      break;
      
      //1 ns
	   case U8X8_MSG_DELAY_NANO:
        delay1us();
      break;
      
    case U8X8_MSG_DELAY_100NANO:
      delay1us();
      break;
      
		case U8X8_MSG_GPIO_AND_DELAY_INIT:  // called once during init phase of u8g2/u8x8
        ssd1306_spi_init();
			break;              // can be used to setup pins
		case U8X8_MSG_GPIO_SPI_CLOCK:        // Clock pin: Output level in arg_int
			if(arg_int)
				digitalWrite(SCK,1);
			else
				digitalWrite(SCK,0);
			break;
		case U8X8_MSG_GPIO_SPI_DATA:        // MOSI pin: Output level in arg_int
			if(arg_int)
				digitalWrite(MOSI,1);
			else
				digitalWrite(MOSI,0);
			break;
		case U8X8_MSG_GPIO_CS:        // CS (chip select) pin: Output level in arg_int
			if(arg_int)
				digitalWrite(CS,1);
			else
				digitalWrite(CS,0);
			break;
		case U8X8_MSG_GPIO_DC:        // DC (data/cmd, A0, register select) pin: Output level in arg_int
      if(arg_int)
				digitalWrite(DC,1);
			else
				digitalWrite(DC,0);
			break;
		
		case U8X8_MSG_GPIO_RESET:     // Reset pin: Output level in arg_int

		default:
		  break;

	}
	return 1;
}


2.u8g2库文件存放位置及引用问题
官方存放位置为mylib 位置；但U8g2库含多个文件 从而其存放为便于管理专门文件夹U8G2（从而放于 x:\天问Block\asrpro\U8G2 下和 x:\天问Block\asrpro\asr_pro_sdk\U8G2 下(位置错误引发的无法找到链接问题，修改位置）

并在source_file.prj中添加相应的头文件地址与库位置
（添加与 source-file 的最后一行）

//U8G2        
source-file: U8G2/u8g2lib.cpp
source-file: U8G2/u8g2_bitmap.c
source-file: U8G2/u8g2_box.c
source-file: U8G2/u8g2_buffer.c
source-file: U8G2/u8g2_button.c
source-file: U8G2/u8g2_circle.c
source-file: U8G2/u8g2_cleardisplay.c
source-file: U8G2/u8g2_d_memory.c
source-file: U8G2/u8g2_d_setup.c
source-file: U8G2/u8g2_font.c
source-file: U8G2/u8g2_fonts.c
source-file: U8G2/u8g2_hvline.c
source-file: U8G2/u8g2_input_value.c
source-file: U8G2/u8g2_intersection.c
source-file: U8G2/u8g2_kerning.c
source-file: U8G2/u8g2_line.c
source-file: U8G2/u8g2_ll_hvline.c
source-file: U8G2/u8g2_message.c
source-file: U8G2/u8g2_polygon.c
source-file: U8G2/u8g2_selection_list.c
source-file: U8G2/u8g2_setup.c
source-file: U8G2/u8x8lib.cpp
source-file: U8G2/u8x8_8x8.c
source-file: U8G2/u8x8_byte.c
source-file: U8G2/u8x8_cad.c
source-file: U8G2/u8x8_capture.c
source-file: U8G2/u8x8_debounce.c
source-file: U8G2/u8x8_display.c
source-file: U8G2/u8x8_d_ssd1306_128x64_noname.c
source-file: U8G2/u8x8_fonts.c
source-file: U8G2/u8x8_gpio.c
source-file: U8G2/u8x8_input_value.c
source-file: U8G2/u8x8_message.c
source-file: U8G2/u8x8_selection_list.c
source-file: U8G2/u8x8_setup.c
source-file: U8G2/u8x8_string.c
source-file: U8G2/u8x8_u16toa.c
source-file: U8G2/u8x8_u8toa.c
source-file: U8G2/myfont1.c


//header file path.
include-path: U8G2

在主程序中引用 

#include "../U8G2/u8g2ASR.h"






移植 u8g2教程 修改 delay
问题：
源文件存放位置 与引用 
对cpp 实现的 无法查找

内存只有2-4M ，处理速度为 ；端口数；通信协议
为弥补asrpro的缺陷，通过与电脑串口连接，传输图片；
将视频文件转为bdf格式，压缩视频
将命令传输至电脑进行处理 
端口的扩展，通过pca9685;扩展，通过arduino stm32扩展
唤醒中的问题
与特定人问题识别；通过使用命令式，减少唤醒与唤醒后才执行命令；wakeup 的时间限制等进行处理


